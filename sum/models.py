from django.db import models


class SumJob(models.Model):
    status = models.CharField(max_length=20)
    result = models.IntegerField(null=True)
    random_number_job_a = models.IntegerField(null=True)
    random_number_job_b = models.IntegerField(null=True)
    number_a = models.IntegerField(null=True)
    number_b = models.IntegerField(null=True)
