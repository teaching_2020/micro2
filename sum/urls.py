from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:sum_job_id>/', views.detail, name='detail'),
        
    path('callback/', views.callback_handler, name='callback'),
    path('post_job/', views.post_job, name='post_job'),

]
