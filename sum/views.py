from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
import requests
from django.conf import settings
from django.urls import reverse
from .models import SumJob
import json
from django.db.models import Q


# Create your views here.
def index(request):
    job_list = [{
        "id": sumJob.id,
        "status": sumJob.status ,
        "random_number_job_a": sumJob.random_number_job_a, 
        "random_number_job_b": sumJob.random_number_job_b, 
        "number_a": sumJob.number_a,
        "number_b": sumJob.number_b,
    } for sumJob in SumJob.objects.all()]

    return JsonResponse({"results": job_list})
 

def detail(request, sum_job_id):
    try:
        sumJob = SumJob.objects.get(pk=sum_job_id)

        return JsonResponse({
            "id": sumJob.id,
            "status": sumJob.status ,
            "random_number_job_a": sumJob.random_number_job_a, 
            "random_number_job_b": sumJob.random_number_job_b, 
            "number_a": sumJob.number_a,
            "number_b": sumJob.number_b,
        })
    except SumJob.DoesNotExist:
        return HttpResponseNotFound()

        # return JsonResponse({
        #     "error": "Unable to match job with ID: {}".format(sum_job_id)
        # }, code=404)



# Create your views here.
def post_job(request):

    callback_url = reverse("callback")
    callback_url = request.build_absolute_uri(callback_url)

    resp_a = requests.post(settings.RANDOM_NUMBER_URL, {
        "callback_url": callback_url,
    })

    resp_b = requests.post(settings.RANDOM_NUMBER_URL, {
        "callback_url": callback_url,
    })

    sumJob = SumJob(
        random_number_job_a=resp_a.text,
        random_number_job_b=resp_b.text,
        status="PENDING"
    )
    sumJob.save()

    return JsonResponse({
        "id": sumJob.id,
        "status": sumJob.status ,
        "random_number_job_a": sumJob.random_number_job_a, 
        "random_number_job_b": sumJob.random_number_job_b, 
        "number_a": sumJob.number_a,
        "number_b": sumJob.number_b,
    })
 


# Create your views here.
def callback_handler(request):
    random_number_job_id = int(request.GET["job_id"])
    result = int(request.GET["result"])

    try:
        sumJob = SumJob.objects.get(
            Q(random_number_job_a=random_number_job_id) | Q(random_number_job_b=random_number_job_id)
        )

        if sumJob.random_number_job_a == random_number_job_id:
            sumJob.number_a = result

        if sumJob.random_number_job_b == random_number_job_id:
            sumJob.number_b = result

        if sumJob.number_a and sumJob.number_b:
            sumJob.result = sumJob.number_a + sumJob.number_b
            sumJob.status = "DONE"

        sumJob.save()

    except SumJob.DoesNotExist:
        print("Unable to match job with ID:", random_number_job_id)

    return HttpResponse("")
 

